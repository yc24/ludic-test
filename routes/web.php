<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsuariosController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('obtener-usuarios-maxjugadas/{acepto}', [UsuariosController::class, 'obtenerUsuariosMaxJugadas']);
Route::get('obtener-usuarios-fecha/{fInicial}/{fFinal}/{letraInicial}', [UsuariosController::class, 'obtenerPorcentajeUsuariosXFecha']);
Route::get('obtener-usuarios-maxpuntos-disfraz/{idDisfraz}', [UsuariosController::class, 'obtenerUsuariosMaxPuntosXIdDisfraz']);
Route::get('obtener-promedio-tiempojuego-idusuario/{idUsuario}', [UsuariosController::class, 'obtenerPromedioTiempoJuegoXIdUsuario']);

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use DateTime;

class UsuariosController extends Controller
{
    /**
 * obtenerUsuariosMaxJugadas
 *
 * Desarrollar el método para obtener un listado de 10 usuarios que jugaron
 * más veces que tengan el parámetro Aceptó en 1 o 0
 * @author Jhon Yaniro
 * @param object $acepto Contiene el valor acepto del usuario
 * @return Collection
 */

    public function obtenerUsuariosMaxJugadas($acepto){
        $usuarios = DB::table('usuarios')->where('Acepto', $acepto)
        ->select('*')->get();

        foreach($usuarios as $usuario){
            $juegos = DB::table('partidas')->where('idJugador', $usuario->id)
            ->select('*')->count();
            $coleccionUsuariosXJugadas[] = ['idUsuario' => $usuario->id,
            'jugadas' => $juegos];
        }

        $coleccionUsuariosXJugadas = collect($coleccionUsuariosXJugadas);
        $coleccionOrdenada = $coleccionUsuariosXJugadas->sortByDesc('jugadas');
        $coleccionUsuariosFinal = $coleccionOrdenada->slice(0, 10);

        return response()->json($coleccionUsuariosFinal, 200);
    }

    /**
 * poblar
 *
 * Método encargado de obtener el porcentaje de usuario que se registraron
 * entre una fecha y otra cuyo nombre empiece con una letra.
 * @author Jhon Yaniro
 * @param object $rangoInicial Contiene el rango inicial de fecha.
 * @param object $rangoFinal Contiene el rango final de fecha.
 * @param object $letraInicial Contiene la primera letra de nombre a filtrar.
 * @return Collection
 */

    public function obtenerPorcentajeUsuariosXFecha($rangoInicial,
    $rangoFinal, $letraInicial){
        $usuarios = DB::table('usuarios')
        ->whereBetween('fechaRegistro',[$rangoInicial, $rangoFinal])
        ->where('Nombre', 'like', $letraInicial.'%')
        ->select('*')->count();

        $usuariosTotales = DB::table('usuarios')
        ->select('id')->count();

        $porcentajeUsuarios = $usuarios / $usuariosTotales * 100;
        $redondeoPorcentaje = round($porcentajeUsuarios, 0)."%";

        return response()->json($redondeoPorcentaje, 200);
    }

    /**
 * poblar
 *
 * Método encargado de obtener los 10 primeros usuarios que ganaron
 * más puntaje filtrando por idDisfraz
 * @author Jhon Yaniro
 * @param object $idDisfraz Contiene el idDisfraz del usuario.
 * @return Collection
 */

    public function obtenerUsuariosMaxPuntosXIdDisfraz($idDisfraz){
        $usuarios = DB::table('usuarios')->where('idDisfraz', $idDisfraz)
        ->select('*')->get();

        foreach($usuarios as $usuario){
            $juegos = DB::table('partidas')->where('idJugador', $usuario->id)
            ->select('*')->sum('puntos');
            $coleccionUsuariosXPuntos[] = ['idUsuario' => $usuario->id,
            'idDisfraz' => $usuario->idDisfraz, 'puntos' => $juegos];
        }

        $coleccionUsuariosXPuntos = collect($coleccionUsuariosXPuntos);
        $coleccionOrdenada = $coleccionUsuariosXPuntos->sortByDesc('puntos');
        $coleccionUsuariosFinal = $coleccionOrdenada->slice(0, 10);

        return response()->json($coleccionUsuariosFinal, 200);
    }

    /**
 * poblar
 *
 * Método encargado de  obtener el promedio de tiempo de juego por usuario
 * @author Jhon Yaniro
 * @param object $idUsuario Contiene el id del usuario.
 * @return String
 */

    public function obtenerPromedioTiempoJuegoXIdUsuario($idUsuario){
            $sumaTiempos = 0;

            $juegos = DB::table('partidas')->where('idJugador', $idUsuario)
            ->select('idJugador', 'fechaInicio', 'fechaFin')->get();
            foreach($juegos as $juego){
                $fechaFin = new DateTime($juego->fechaFin);
                $fechaInicio = new DateTime($juego->fechaInicio);
                $diferencia = date_diff($fechaFin, $fechaInicio);
                $restaFormato = '%s';
                $sumaTiempos = $sumaTiempos + $diferencia->format($restaFormato);
            }
            $promedioTiempo = $sumaTiempos / count($juegos);

        return response()->json($promedioTiempo, 200);
    }


}
